
update loyalty_bi_analytics.sd_data_loaded set "desc" = 'Store-dash FW Failed';

 --- bi_article_sales_temp2 population --

 ;

 ;
   delete from loyalty_bi_analytics.smd_dim_store;

  insert into loyalty_bi_analytics.smd_dim_store
    select bi.store_nbr, max(bi.store_name),
    case
    when max(case when bi.district_code is not null then bi.district_code else bi.state end) = 'ACT' then 'Australian Capital Territory'
    when max(case when bi.district_code is not null then bi.district_code else bi.state end) = 'NSW' or bi.store_nbr = 2689 then 'New South Wales'
    when max(case when bi.district_code is not null then bi.district_code else bi.state end) = 'NT' then 'Northern Territory'
    when max(case when bi.district_code is not null then bi.district_code else bi.state end)= 'QLD' and bi.store_nbr != 2689 then 'Queensland'
    when max(case when bi.district_code is not null then bi.district_code else bi.state end) = 'SA' then 'South Australia'
    when max(case when bi.district_code is not null then bi.district_code else bi.state end) = 'TAS' then 'Tasmania'
    when max(case when bi.district_code is not null then bi.district_code else bi.state end) = 'VIC' then 'Victoria'
    when max(case when bi.district_code is not null then bi.district_code else bi.state end) = 'WA' then 'Western Australia'
    end as state_name,
    max(case when b.grouparea_name is not null then bi.grouparea_name else bi.grouparea_name end)
    ,max(case when b.zone_name is not null then bi.zone_name else bi.zone_name end),
    'WOOLWORTHS' as banner,
      max(case when b.stateshort is not null then b.stateshort else bi.state end)
      , max(a.latitude)
      , mAx(a.longitude)
      , max(a.sa1_maincode_2016)
      , max(a.sa1_7digitcode_2016)
      , max(a.sa2_maincode_2016)
      , max(a.sa2_5digitcode_2016)
    from loyalty.location bi
      left join loyalty_bi_analytics.smd_update_stor_info b
      on bi.store_nbr = b.store_nbr
    left join loyalty_bi_analytics.competitor_location_census_bi a
    on bi.store_nbr = a.store_nbr
    and upper(a.banner) = 'WOOLWORTHS'
    where bi.store_nbr <> -100
    and division_nbr = 1005
    group by 1
    union all
    select case
    when upper(a.banner) = 'WOOLWORTHS' then a.store_nbr::int
    when upper(a.banner) = 'COLES' then 100000 + a.store_nbr::int
    when upper(a.banner) = 'IGA' then 200000 + a.store_nbr::int
    when upper(a.banner) = 'ALDI' then 300000 + row_number() over (PARTITION BY a.banner order by a.storename)
    end as store_nbr,
    a.storename,
    case
    when a.stateshort = 'ACT' then 'Australian Capital Territory'
    when a.stateshort = 'NSW' then 'New South Wales'
    when a.stateshort = 'NT' then 'Northern Territory'
    when a.stateshort = 'QLD' then 'Queensland'
    when a.stateshort = 'SA' then 'South Australia'
    when a.stateshort = 'TAS' then 'Tasmania'
    when a.stateshort = 'VIC' then 'Victoria'
    when a.stateshort = 'WA' then 'Western Australia'
    end as state_name, null, null,
    a.banner, a.stateshort, a.latitude, a.longitude, a.sa1_maincode_2016, a.sa1_7digitcode_2016, a.sa2_maincode_2016, a.sa2_5digitcode_2016
    from loyalty_bi_analytics.competitor_location_census_bi a
    where upper(a.banner) <> 'WOOLWORTHS';


update loyalty_bi_analytics.smd_dim_store
set state_name = 'Customer Fullfillment Center'
,grouparea_name =  'CFC Group'
, zone_name = 'CFC Zone'
where store_nbr in (1800,1393,3800)
;
 truncate table loyalty_bi_analytics.vw_smd_dim_store;
 insert into loyalty_bi_analytics.vw_smd_dim_store

 SELECT derived_table1.store_nbr, derived_table1.store_name, derived_table1.state_name, derived_table1.grouparea_name, derived_table1.zone_name, derived_table1.banner, derived_table1.stateshort, derived_table1.latitude, derived_table1.longitude, derived_table1.sa1_maincode_2016, derived_table1.sa1_7digitcode_2016, derived_table1.sa2_maincode_2016, derived_table1.sa2_5digitcode_2016, derived_table1.country, derived_table1.manager_name, derived_table1.manager_email, "replace"(derived_table1.sa2_name::text, ','::character varying::text, ' '::character varying::text)::character varying(30) AS sa2_name
   FROM ( SELECT smd_dim_store.store_nbr, "max"(smd_dim_store.store_name::text)::character varying(80) AS store_name, "max"(smd_dim_store.state_name::text)::character varying(28) AS state_name,
                CASE
                    WHEN len("max"(smd_dim_store.grouparea_name::text)::character varying::text) = 0 OR "max"(smd_dim_store.grouparea_name::text) IS NULL THEN 'Group999'::character varying::character varying(50)::text
                    ELSE "max"(smd_dim_store.grouparea_name::text)
                END::character varying(50) AS grouparea_name,
                CASE
                    WHEN len("max"(smd_dim_store.zone_name::text)::character varying::text) = 0 OR "max"(smd_dim_store.zone_name::text) IS NULL THEN 'Zone999'::character varying::character varying(50)::text
                    ELSE "max"(smd_dim_store.zone_name::text)
                END::character varying(50) AS zone_name, "max"(smd_dim_store.banner::text)::character varying(30) AS banner, "max"(smd_dim_store.stateshort::text)::character varying(4) AS stateshort, "max"(smd_dim_store.latitude) AS latitude, "max"(smd_dim_store.longitude) AS longitude, "max"(smd_dim_store.sa1_maincode_2016) AS sa1_maincode_2016, "max"(smd_dim_store.sa1_7digitcode_2016) AS sa1_7digitcode_2016, "max"(smd_dim_store.sa2_maincode_2016) AS sa2_maincode_2016, "max"(smd_dim_store.sa2_5digitcode_2016) AS sa2_5digitcode_2016, 'AU'::character varying AS country, "max"(COALESCE(smd_dim_store_contact.manager_name, ''::character varying)::text)::character varying(100) AS manager_name, "max"(COALESCE(smd_dim_store_contact.manager_email, ''::character varying)::text)::character varying(100) AS manager_email, "max"(a.sa2_name)::character varying(30) AS sa2_name
           FROM loyalty_bi_analytics.smd_dim_store
      LEFT JOIN loyalty_bi_analytics.smd_dim_store_contact ON smd_dim_store.store_nbr = smd_dim_store_contact.store_nbr
   LEFT JOIN ( SELECT competitor_location_census_bi.sa2_5digitcode_2016, "max"(competitor_location_census_bi.suburb::text) AS sa2_name
              FROM loyalty_bi_analytics.competitor_location_census_bi
             GROUP BY competitor_location_census_bi.sa2_5digitcode_2016) a ON smd_dim_store.sa2_5digitcode_2016 = a.sa2_5digitcode_2016
  GROUP BY smd_dim_store.store_nbr) derived_table1
  WHERE derived_table1.grouparea_name::text = 'Group999'::character varying::text AND derived_table1.banner::text <> 'WOOLWORTHS'::text OR derived_table1.grouparea_name::text <> 'Group999'::character varying::text AND derived_table1.banner::text = 'WOOLWORTHS'::text;

 ;delete from loyalty_bi_analytics.bi_article_sales
    where start_txn_date between (
   select  (trunc(fw_end_date)-6) ::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate())))
    ) and
    (
   select  (trunc(fw_end_date)) ::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate())))
    )
    ;
   insert into loyalty_bi_analytics.bi_article_sales
    with cvm_pw_end_date as (
      --set up the cvm_pw_end date, eg. for fw end at Feb 4th, the cvm_pw_end_date should be Jan 30th, they are in the same Financial Week--

	select  trunc(pw_end_date)::date AS cvm_pw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-10,dateadd(hour,10,getdate())))

    ),

    financial_week_end as (
    --set up the fw_end_date, eg. latest sunday which is the latest financial week end date, eg Feb 4th 2018
   select  trunc(fw_end_date) ::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate())))

    ),
        sd_customer_value_model2 AS
    (select pw_end_date  as pw_end_date , crn, affluence, lifestage, macro_segment_curr, (1-stretch_perc_raw) as sow, preferred_store
    from customer_value_model cvm cross join cvm_pw_end_date
    where pw_end_date = cvm_pw_end_date.cvm_pw_end_date)
 ,
    sd_customer_tender_card_linkage as (
        SELECT DISTINCT
    start_txn_date
    ,checkout_nbr
    ,tkt_nbr
    ,store_nbr
    ,start_txn_time
    ,linked_lylty_card_nbr
    FROM loyalty_bi_analytics.vw_customer_tender_card_linkage a
      cross join financial_week_end b
    WHERE a.START_TXN_DATE BETWEEN b.fw_end_date - 6  and b.fw_end_date

      )
    ,
    sd_ass as (

    select start_txn_date, start_txn_time, division_nbr, checkout_nbr, store_nbr
	--, tkt_nbr
	, case when tkt_nbr > 10000000 then tkt_nbr/10000::smallint else tkt_nbr end as tkt_nbr
	, lylty_card_nbr, basket_key, prod_nbr, tot_measured_qty, prod_qty, tot_amt_incld_gst, tot_wow_dollar_incld_gst, gst_pct
    from article_sales_summary ass
      cross join financial_week_end b
    WHERE ass.START_TXN_DATE BETWEEN b.fw_end_date - 6  and b.fw_end_date
    and (ass.division_nbr = 1005 OR ass.division_nbr = 1030)
    AND ass.void_flag <> 'Y'
      )
    ,
    TP AS
    (
    select distinct clndr_date as period_id, trunc(dim_date.fw_end_date) as fw_end_date, trunc(pw_end_date) as pw_end_date
    from dim_Date CROSS JOIN financial_week_end b
    where trunc(dim_date.fw_end_date) = b.fw_end_date
    )



    select
    ass.start_txn_date
    , ass.start_txn_time
    , tp.pw_end_date
    , tp.fw_end_date
    , ass.division_nbr
    , loc.district_code as state
    , case when ass.checkout_nbr = 100 then -100 else ass.store_nbr end as store_nbr
    , case when ass.checkout_nbr = 100 then 'Online Store' else loc.store_name end as store_name
    , ass.store_nbr as store_nbr_all
    , loc.store_name as store_name_all
    , ass.tkt_nbr
    , ass.checkout_nbr
    , CASE
    WHEN ass.lylty_card_nbr not in ('0') THEN nvl(NULLIF(HST1.crn,0),'000')
    when ass.lylty_card_nbr = '0' AND ctcl.linked_lylty_card_nbr IS NOT NULL then nvl(NULLIF(hist.crn,0),'000')
    else '000'
    end as crn
    , CASE
    WHEN ass.lylty_card_nbr = '0' AND ctcl.linked_lylty_card_nbr IS NOT NULL THEN 1
    ELSE 0
    END AS flux_flag
    , CASE
    WHEN ass.lylty_card_nbr = '0' AND ctcl.linked_lylty_card_nbr IS NOT NULL THEN ctcl.linked_lylty_card_nbr
    WHEN ass.lylty_card_nbr not in ('0') THEN ass.lylty_card_nbr
    ELSE '0'
    END AS lylty_card_nbr_inc_flux
    , case when pk.basket_key is not null then 1 else 0 end as pickup_flag
    , coalesce(NULLIF(cvm.affluence,''),'UNCLASSIFIED') as affluence
    , coalesce(NULLIF(cvm.lifestage,''),'UNCLASSIFIED') as life_stage
    , coalesce(NULLIF(cvm.macro_segment_curr,''),'UNCLASSIFIED') as macro_segment_curr
    , NULLIF(preferred_store,'')::int
    , general_mgr_username
    , head_of_trade_username
    , buying_ctgry_mgr_username
    , am.dept_name
    , am.ctgry_name
    , am.subcat_name
    , ass.basket_key
    , cvm.sow
    , ass.prod_nbr
    , SUM(CASE WHEN TOT_MEASURED_QTY > 0 THEN TOT_MEASURED_QTY ELSE PROD_QTY END) prod_qty
    , sum((nvl(ass.TOT_AMT_INCLD_GST,0)- nvl(ass.tot_wow_dollar_incld_gst,0))/(1+(nvl(ass.gst_pct,0)/100))) as tot_amt_excld_gst_wo_wow
    , sum((nvl(ass.TOT_AMT_INCLD_GST,0))/(1+(nvl(ass.gst_pct,0)/100))) as tot_amt_excld_gst_wt_wow
    , sum(nvl(ass.TOT_AMT_INCLD_GST,0) ) as tot_amt_incld_gst
    , sum(ass.tot_wow_dollar_incld_gst  )
    FROM  tp
    inner join sd_ass ass
    on ass.start_txn_date = tp.period_id
    INNER JOIN article_master am
    ON ass.prod_nbr = am.prod_nbr
    AND ass.division_nbr = am.division_nbr
    INNER JOIN loyalty_bi_analytics.location_bi loc
    ON ass.store_nbr = loc.store_nbr
    LEFT JOIN sd_customer_tender_card_linkage ctcl
    ON ass.start_txn_date = ctcl.start_txn_date
    AND ass.checkout_nbr = ctcl.checkout_nbr
    AND ass.tkt_nbr = ctcl.tkt_nbr
    AND ass.store_nbr = ctcl.store_nbr
    AND ass.start_txn_time = ctcl.start_txn_time
    LEFT JOIN loyalty_bi_analytics.vw_dim_lylty_card_hist_v hst1
    ON (hst1.lylty_card_nbr = ass.lylty_card_nbr)
    AND (ass.start_txn_date >= hst1.eff_date and  ass.start_txn_date < hst1.exp_date)
    LEFT JOIN loyalty_bi_analytics.vw_dim_lylty_card_hist_v hist
    ON (hist.lylty_card_nbr = ctcl.linked_lylty_card_nbr)
    AND (ass.start_txn_date >= hist.eff_date and  ass.start_txn_date < hist.exp_date)
    LEFT JOIN sd_customer_value_model2 cvm
    on CASE
    WHEN ass.lylty_card_nbr not in ('0') THEN HST1.crn
    when ass.lylty_card_nbr = '0' AND ctcl.linked_lylty_card_nbr IS NOT NULL then hist.crn
    else '0'
    end = cvm.crn
    LEFT JOIN loyalty_bi_analytics.online_order_pickup pk
    ON ass.basket_key = pk.basket_key
    WHERE am.dept_code <> 20
    GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29
    order by 13, 1, 4, 6, 7;

---- For Populating the loyalty_bi_analytics.smd_temp3---- replace %s1 with the latest fw_end_date, do check if the data is populated or not before inserting
  delete from loyalty_bi_analytics.smd_temp3
    where pw_end_date =

    (
   select  (trunc(fw_end_date)) ::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate())))
    )
 ;
  insert into loyalty_bi_analytics.smd_temp3
     with financial_week_end as (
    --set up the fw_end_date, eg. latest sunday which is the latest financial week end date, eg Feb 4th 2018
   select  trunc(fw_end_date)::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate())))

    )
          select
         f.fw_end_date,
         'Financial Week',
         b.store_nbr,
         case when f.store_nbr = -100 then 1 else 0 end as online_flag,
         pickup_flag,
         life_stage,
         affluence,
         crn,
         flux_flag,
         case when macro_segment_curr in ('HVHIGH', 'HVMED', 'MVHIGH', 'MVMEDA') then 1 else 0 end as valuable_flag,
         sum(f.tot_amt_excld_gst_wt_wow) as total_sales,
         sum(case when f.lylty_card_nbr_inc_flux != '0' and flux_flag = 0 then tot_amt_excld_gst_wt_wow end ) as total_sales_scan,
         sum(case when f.crn != '000' then tot_amt_excld_gst_wt_wow end ) as total_sales_flux,
         count(distinct basket_key) as basket_count,
         count(distinct case when f.crn != '000' and flux_flag = 0 then basket_key end ) as basket_count_scan,
         count(distinct case when f.crn != '000' then basket_key end ) as basket_count_flux,
         sum(prod_qty) as prod_qty,
         sum(case when f.crn != '000' and flux_flag = 0 then prod_qty end ) as prod_qty_scan,
         sum(case when f.crn != '000' then prod_qty end ) as prod_qty_flux
         ,avg(sow) --available when fact table has this value
         ,sum(case when f.lylty_card_nbr_inc_flux != '0' and flux_flag = 0 then tot_amt_excld_gst_wt_wow end ) as total_sales_scan_lylty
         ,count(distinct case when  f.lylty_card_nbr_inc_flux != '0' and flux_flag = 0 then basket_key end ) as basket_count_scan_lylty
         ,sum(case when b.stateshort != 'TAS' then f.tot_amt_excld_gst_wt_wow end ) as tag_total_sales
         ,sum(case when f.lylty_card_nbr_inc_flux != '0' and flux_flag = 0 and b.stateshort != 'TAS' then tot_amt_excld_gst_wt_wow end ) as tag_total_sales_scan
         ,count(distinct case when b.stateshort != 'TAS' then basket_key end) as scan_rate_total_count
         ,count(distinct case when f.lylty_card_nbr_inc_flux != '0' and flux_flag = 0 and b.stateshort != 'TAS' then basket_key end) as scan_rate_total_scan_count
         from loyalty_bi_analytics.bi_article_sales f
         inner join loyalty_bi_analytics.vw_smd_dim_store b
         on f.store_nbr_all = b.store_nbr cross join financial_week_end
         where  f.fw_end_date = financial_week_end.fw_end_date
         and f.department not in ('NON TRADING','NON TRADE')
         group by 1,3,4,5,6,7,8,9,10;


---- For Populating the loyalty_bi_analytics.smd_temp5---- replace %s1 with the latest fw_end_date, do check if the data is populated or not before inserting

    delete from loyalty_bi_analytics.smd_temp5
    where start_txn_date between (
   select  (trunc(fw_end_date)-4) ::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate())))
    ) and
    (
   select  (trunc(fw_end_date)) ::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate()))))
 ;

   insert into loyalty_bi_analytics.smd_temp5
     with financial_week_end as (
    --set up the fw_end_date, eg. latest sunday which is the latest financial week end date, eg Feb 4th 2018
    select  trunc(fw_end_date)::date as fw_end_date
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 ,dateadd(hour,10,getdate())))

    )
    select
    start_txn_date,
    f.fw_end_date,
    'Financial Week',
    b.store_nbr,
    case when f.checkout_nbr between 60 and 99 then 1 else 0 end

    ,sum(case when b.stateshort!= 'TAS' then f.tot_amt_excld_gst_wt_wow end ) as tag_total_sales
    ,sum(case when f.lylty_card_nbr_inc_flux != '0' and flux_flag = 0 and b.stateshort != 'TAS' then tot_amt_excld_gst_wt_wow end ) as tag_total_sales_scan
    ,count(distinct case when b.stateshort != 'TAS' then basket_key end) as scan_rate_total_count
    ,count(distinct case when f.lylty_card_nbr_inc_flux != '0' and flux_flag = 0 and b.stateshort != 'TAS' then basket_key end) as scan_rate_total_scan_count
    from loyalty_bi_analytics.bi_article_sales f
    inner join loyalty_bi_analytics.vw_smd_dim_store b
    on f.store_nbr_all = b.store_nbr cross join financial_week_end
    where
    f.start_txn_date between financial_week_end.fw_end_date - 6 and financial_week_end.fw_end_date 
    and f.department not in ('NON TRADING','NON TRADE')
    group by 1,2,4,5;


 ---- Deleting Data, not need to change anything--------
    truncate loyalty_bi_analytics.smd_rel_time_period_group_day;

    delete from loyalty_bi_analytics.competitor_location_census_bi;

    delete from loyalty_bi_analytics.competitor_location_census_bi
    where store_nbr = 5680 and competitorlocationid = 3555
    or store_nbr = 1102 and competitorlocationid = 1279
    or store_nbr = 5702 and competitorlocationid = 2219;

    delete from loyalty_bi_analytics.smd_rel_time_period_group;

    delete from loyalty_bi_analytics.smd_temp4;

    delete from loyalty_bi_analytics.smd_active_member;

    delete from loyalty_bi_analytics.smd_temp4b;

    delete from loyalty_bi_analytics.smd_first_scan ;

    delete from loyalty_bi_analytics.smd_unreg ;

    delete from LOYALTY_BI_ANALYTICS.smd_member;

    delete from loyalty_bi_analytics.smd_comp;

    truncate table loyalty_bi_analytics.smd_sales;

    delete from loyalty_bi_analytics.smd_mapping_mx_Cnt1;

    delete from loyalty_bi_analytics.smd_mapping ;



    delete from loyalty_bi_analytics.smd_temp4c;

    delete from loyalty_bi_analytics.smd_rel_time_period_group_month;

    delete from loyalty_bi_analytics.smd_member_month;

    delete from loyalty_bi_analytics.smd_sales_month;

    delete from loyalty_bi_analytics.smd_temp4d;

    delete from loyalty_bi_analytics.smd_scan_tag_daily;

    delete from loyalty_bi_analytics.smd_scan_tag_by_service;

 -----populate store_dash tables------;
 ;



    insert into loyalty_bi_analytics.smd_rel_time_period_group_day
    with a as (
    select distinct 'Last Week vs SPLY' as time_period_group,  'TP' as metric_type, trunc(fw_end_date) as fw_end_date,'Financial Week' as week_type
    from dim_date
    where clndr_date = trunc(dateadd(day,-7,dateadd(hour,10,getdate())))
    union
      select distinct (dense_rank() over (order by fw_end_date desc) + 1)::varchar + ' Weeks ago' as time_period_group,  'TP' as metric_type, trunc(fw_end_date) as fw_end_date,'Financial Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14,dateadd(hour,10,getdate())))
      )

    select a.time_period_group,a.metric_type,dd.fw_end_date,a.week_type,dd.clndr_date
    from dim_date dd
      cross join a
    where dd.clndr_date between dateadd(day, -29, a.fw_end_date) and a.fw_end_date

    ;

   insert into loyalty_bi_analytics.smd_rel_time_period_group_day
      with b as (
        select distinct 'Last Week vs SPLY' as time_period_group,  'TP' as metric_type, case when date_part(dow,dateadd(hour,11,getdate())) >= 3 then trunc(fw_end_date) + 2 else trunc(fw_end_date) -5 end as pw_end_date ,'Promotion Week' as week_type from dim_date
    where clndr_date = trunc(dateadd(day,-7,dateadd(hour,10,getdate())))
        union
      select distinct (dense_rank() over (order by pw_end_date desc) + 1)::varchar + ' Weeks ago' ,  'TP' as metric_type, trunc(pw_end_date) as fw_end_date,'Promotion Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14,dateadd(hour,10,getdate())))

      )

      select b.time_period_group,b.metric_type,dd.pw_end_date,b.week_type,dd.clndr_date
    from dim_date dd
      cross join b
    where dd.clndr_date between dateadd(day, -29, b.pw_end_date) and b.pw_end_date

    ;


    insert into loyalty_bi_analytics.smd_rel_time_period_group_day
    with a as (
    select distinct 'Last Week vs SPLY' as time_period_group,  'LP' as metric_type, trunc(fw_end_date) as fw_end_date,'Financial Week' as week_type
    from dim_date
    where clndr_date = trunc(dateadd(day,-7 - 52*7 ,dateadd(hour,10,getdate())))

    union all

    select distinct (dense_rank() over (order by  trunc(fw_end_date) desc) + 1)::varchar + ' Weeks ago' ::varchar as time_period_group,  'LP' as metric_type, trunc(fw_end_date) as fw_end_date,'Financial Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28 - 52*7,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14 - 52*7,dateadd(hour,10,getdate())))
      )

    select a.time_period_group,a.metric_type,dd.fw_end_date,a.week_type,dd.clndr_date
    from dim_date dd
      cross join a
    where dd.clndr_date between dateadd(day, -29, a.fw_end_date) and a.fw_end_date

    ;
    insert into loyalty_bi_analytics.smd_rel_time_period_group_day
      with b as (
        select distinct 'Last Week vs SPLY' as time_period_group,  'LP' as metric_type, case when date_part(dow,dateadd(hour,11,getdate())) >= 3 then trunc(fw_end_date) + 2 else trunc(fw_end_date) -5 end as pw_end_date ,'Promotion Week' as week_type from dim_date
    where clndr_date = trunc(dateadd(day,-7 - 52*7 ,dateadd(hour,10,getdate())))
     union
      select distinct (dense_rank() over (order by pw_end_date desc) + 1)::varchar + ' Weeks ago'  as time_period_group,  'LP' as metric_type, trunc(pw_end_date) as fw_end_date,'Promotion Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28-52*7,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14-52*7,dateadd(hour,10,getdate())))

      )

      select b.time_period_group,b.metric_type,dd.pw_end_date,b.week_type,dd.clndr_date
    from dim_date dd
      cross join b
    where dd.clndr_date between dateadd(day, -29, b.pw_end_date) and b.pw_end_date



    ;


    insert into loyalty_bi_analytics.smd_rel_time_period_group_day
    with a as (
    select distinct 'Last Month vs SPLY' as time_period_group,  'TP' as metrict_type, trunc(a.fw_start_date) + 8 as pw_end_date,'Promotion Week' as week_type,a.clndr_date as clndr_date
    from dim_date a
    inner join
    (
    select fy, pofy from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1

    union all

    select distinct 'Last Month vs SPLY',  'LP', trunc(a.fw_start_date) + 8,'Promotion Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy-1 as fy, pofy from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1

    union all

    select distinct 'Last Month vs SPLY',  'TP', trunc(a.fw_start_date) + 6 ,'Financial Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy, pofy from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1

    union all

    select distinct 'Last Month vs SPLY',  'LP', trunc(a.fw_start_date) + 6 ,'Financial Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy-1 as fy, pofy from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1


    union all
    --quarter
    select distinct 'Last Quarter vs SPLY',  'TP', trunc(a.fw_start_date) + 8,'Promotion Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy, qofy
    from dim_date
    where clndr_date = next_day(getDate() ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    union all

    select distinct 'Last Quarter vs SPLY',  'LP', trunc(a.fw_start_date) + 8,'Promotion Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy-1 as fy, qofy
    from dim_date
    where clndr_date = next_day(getDate() ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    UNION ALL
    select distinct 'Last Quarter vs SPLY',  'TP', trunc(a.fw_start_date) + 6,'Financial Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy, qofy
    from dim_date
    where clndr_date = next_day(getDate()   ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    union all

    select distinct 'Last Quarter vs SPLY',  'LP', trunc(a.fw_start_date) + 6 ,'Financial Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy-1 as fy, qofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    union all
    --half
    select distinct 'Last Half vs SPLY',  'TP', trunc(a.fw_start_date) + 8,'Promotion Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy, hofy
    from dim_date
    where clndr_date = next_day(getDate() ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1

    union all

    select distinct 'Last Half vs SPLY',  'LP', trunc(a.fw_start_date) + 8,'Promotion Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy-1 as fy, hofy from dim_date
    where clndr_date = next_day(getDate() ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1

    UNION ALL
    select distinct 'Last Half vs SPLY',  'TP', trunc(a.fw_start_date) + 6 ,'Financial Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy, hofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1

    union all

    select distinct 'Last Half vs SPLY',  'LP', trunc(a.fw_start_date) + 6 , 'Financial Week',a.clndr_date
    from dim_date a
    inner join
    (
    select fy-1 as fy, hofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1)


     select *
    from a;


    insert into loyalty_bi_analytics.competitor_location_census_bi
    select * from competitor_location_census;

    update loyalty_bi_analytics.competitor_location_census_bi
    set stateshort = district_code
    from loyalty_bi_analytics.competitor_location_census_bi a
    inner join (select distinct store_nbr, district_code from loyalty_bi_analytics.location_bi) b
    on a.store_nbr = b.store_nbr
    and upper(a.banner) = 'WOOLWORTHS';

    insert into loyalty_bi_analytics.smd_rel_time_period_group
    select distinct 'Last Week vs SPLY',  'TP', trunc(fw_end_date),'Financial Week'
    from dim_date
    where clndr_date = trunc(dateadd(day,-7,dateadd(hour,10,getdate())))

    union all

    select distinct (dense_rank() over (order by fw_end_date desc) + 1)::varchar + ' Weeks ago'  as time_period_group,  'TP' as metric_type, trunc(fw_end_date) as fw_end_date,'Financial Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14,dateadd(hour,10,getdate())))

    union all

    select distinct 'Last Week vs SPLY',  'LP', trunc(fw_end_date),'Financial Week'
    from dim_date
    where clndr_date = trunc(dateadd(week,-52,dateadd(day,-7,dateadd(hour,10,getdate()))))

    union all

       select distinct (dense_rank() over (order by fw_end_date desc) + 1)::varchar + ' Weeks ago'  as time_period_group,  'LP' as metric_type, trunc(fw_end_date) as fw_end_date,'Financial Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28 - 52*7,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14 - 52*7 ,dateadd(hour,10,getdate())))


    UNION ALL

    select distinct 'Last Week vs SPLY',  'TP', case when date_part(dow,dateadd(hour,11,getdate())) >= 3 then trunc(fw_end_date) + 2 else trunc(fw_end_date) -5 end ,'Promotion Week' from dim_date
    where clndr_date = trunc(dateadd(day,-7,dateadd(hour,10,getdate())))

    union all

    select distinct  (dense_rank() over (order by dim_date.pw_end_date desc) + 1)::varchar + ' Weeks ago'  as time_period_group,  'TP' as metric_type, trunc(pw_end_date) as fw_end_date,'Promotion Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14,dateadd(hour,10,getdate())))

    union all

    select distinct 'Last Week vs SPLY',  'LP',case when date_part(dow,dateadd(hour,11,getdate())) >= 3 then trunc(fw_end_date) + 2 else trunc(fw_end_date) -5 end,'Promotion Week'  from dim_date
    where  clndr_date = trunc(dateadd(week,-52,dateadd(day,-7,dateadd(hour,10,getdate()))))

    union all

    select distinct (dense_rank() over (order by dim_date.pw_end_date desc) + 1)::varchar + ' Weeks ago' as time_period_group,  'LP' as metric_type, trunc(pw_end_date) as fw_end_date,'Promotion Week' as week_type
    from dim_date
    where clndr_date between trunc(dateadd(day,-28 - 52*7,dateadd(hour,10,getdate()))) and  trunc(dateadd(day,-14 - 52*7 ,dateadd(hour,10,getdate())))

    union all

    --month
    select distinct 'Last Month vs SPLY',  'TP', trunc(a.fw_start_date) + 8,'Promotion Week'
    from dim_date a
    inner join
    (
    select fy, pofy from dim_date
    where clndr_date = next_day(getDate()   ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1

    union all

    select distinct 'Last Month vs SPLY',  'LP', trunc(a.fw_start_date) + 8,'Promotion Week'
    from dim_date a
    inner join
    (
    select fy-1 as fy, pofy from dim_date
    where clndr_date = next_day(getDate() ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1

    union all

    select distinct 'Last Month vs SPLY',  'TP', trunc(a.fw_start_date) + 6 ,'Financial Week'
    from dim_date a
    inner join
    (
    select fy, pofy from dim_date
    where clndr_date = next_day(getDate()   ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1

    union all

    select distinct 'Last Month vs SPLY',  'LP', trunc(a.fw_start_date) + 6 ,'Financial Week'
    from dim_date a
    inner join
    (
    select fy-1 as fy, pofy from dim_date
    where clndr_date = next_day(getDate() ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.pofy = b.pofy - 1 and b.pofy > 1
    or
    a.fy = b.fy - 1 and a.pofy = 12 and b.pofy = 1


    union all
    --quarter
    select distinct 'Last Quarter vs SPLY',  'TP', trunc(a.fw_start_date) + 8,'Promotion Week'
    from dim_date a
    inner join
    (
    select fy, qofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    union all

    select distinct 'Last Quarter vs SPLY',  'LP', trunc(a.fw_start_date) + 8,'Promotion Week'
    from dim_date a
    inner join
    (
    select fy-1 as fy, qofy
    from dim_date
    where clndr_date = next_day(getDate() ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    UNION ALL
    select distinct 'Last Quarter vs SPLY',  'TP', trunc(a.fw_start_date) + 6,'Financial Week'
    from dim_date a
    inner join
    (
    select fy, qofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    union all


    select distinct 'Last Quarter vs SPLY',  'LP', trunc(a.fw_start_date) + 6 ,'Financial Week'
    from dim_date a
    inner join
    (
    select fy-1 as fy, qofy
    from dim_date
    where clndr_date = next_day(getDate()   ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.qofy = b.qofy - 1 and b.qofy > 1
    or
    a.fy = b.fy - 1 and a.qofy = 4 and b.qofy = 1

    union all
    --half
    select distinct 'Last Half vs SPLY',  'TP', trunc(a.fw_start_date) + 8,'Promotion Week'
    from dim_date a
    inner join
    (
    select fy, hofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1

    union all

    select distinct 'Last Half vs SPLY',  'LP', trunc(a.fw_start_date) + 8,'Promotion Week'
    from dim_date a
    inner join
    (
    select fy-1 as fy, hofy from dim_date
    where clndr_date = next_day(getDate()   ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1

    UNION ALL
    select distinct 'Last Half vs SPLY',  'TP', trunc(a.fw_start_date) + 6 ,'Financial Week'
    from dim_date a
    inner join
    (
    select fy, hofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1

    union all

    select distinct 'Last Half vs SPLY',  'LP', trunc(a.fw_start_date) + 6 , 'Financial Week'
    from dim_date a
    inner join
    (
    select fy-1 as fy, hofy
    from dim_date
    where clndr_date = next_day(getDate()  ,'Sunday')
    ) b
    on
    a.fy = b.fy and a.hofy = b.hofy - 1 and b.hofy > 1
    or
    a.fy = b.fy - 1 and a.hofy = 2 and b.hofy = 1
    ;

    insert into loyalty_bi_analytics.smd_temp4
    with weekly as
    (
    select week_type,pw_end_date, store_nbr, online_flag, life_stage,pickup_flag, affluence, valuable_flag, count(distinct crn) as shopper_count,
    sum(total_sales) as total_sales,
    --sum(total_sales_scan) as total_sales_scan,
    sum(total_sales_scan_lylty) as total_sales_scan,
    sum(total_sales_flux) as total_sales_flux,

    sum(basket_count) as basket_count,
    --sum(basket_count_scan) as basket_count_scan,
    sum(basket_count_scan_lylty) as basket_count_scan,
    sum(basket_count_flux) as basket_count_flux,

    sum(prod_qty) as prod_qty,
    sum(prod_qty_scan) as prod_qty_scan,
    sum(prod_qty_flux) as prod_qty_flux,
    avg(sow) as sow --available when sow is in fact table
    ,sum(tag_total_sales) as tag_rate_total_sales
    ,sum(tag_total_sales_scan) as tag_rate_total_sales_scan
    ,sum(scan_rate_total_count) as scan_rate_basket_count
    ,sum(scan_rate_total_scan_count) as scan_rate_basket_count_scan

    from loyalty_bi_analytics.smd_temp3
    --where total_sales>0
    group by 1,2,3,4,5,6,7,8
    )
    select
      tpg.time_period_group
      , tpg.metric_type
      , f.week_type,
    f.store_nbr,
    f.online_flag,
    f.pickup_flag,
    f.life_stage,
    f.affluence,
    f.valuable_flag,
    sum(shopper_count) as shopper_count,
    sum(total_sales) as total_sales,
    sum(total_sales_scan) as total_sales_scan,
    sum(total_sales_flux) as total_sales_flux,

    sum(basket_count) as basket_count,
    sum(basket_count_scan) as basket_count_scan,
    sum(basket_count_flux) as basket_count_flux,

    sum(prod_qty) as prod_qty,
    sum(prod_qty_scan) as prod_qty_scan,
    sum(prod_qty_flux) as prod_qty_flux,
    avg(sow) as sow, --available when sow is in fact table
    count(distinct tpg.pw_end_date),
    sum(tag_rate_total_sales),
    sum(tag_rate_total_sales_scan),
    sum(scan_rate_basket_count),
    sum(scan_rate_basket_count_scan)
    from
    weekly f
    inner join loyalty_bi_analytics.smd_rel_time_period_group tpg
    on f.pw_end_date = tpg.pw_end_date
    and f.week_type = tpg.week_type
    group by 1,2,3,4,5,6,7,8,9
    ;

    INSERT INTO loyalty_bi_analytics.smd_active_member (metric_type, time_period_group, store_nbr, active_member_count)
      WITH active_members AS (
              SELECT
                srtpg.metric_type,
                srtpg.time_period_group,
                st3.store_nbr,
                crn,
                row_number()
                OVER (
                  PARTITION BY srtpg.time_period_group, srtpg.metric_type, crn
                  ORDER BY srtpg.pw_end_date DESC ) AS row_nbr
              FROM loyalty_bi_analytics.smd_temp3 st3
                INNER JOIN (SELECT *
                            FROM loyalty_bi_analytics.smd_rel_time_period_group
                            WHERE week_type = 'Promotion Week'
                           ) srtpg
                  ON st3.pw_end_date BETWEEN dateadd(WEEK, -13, srtpg.pw_end_date) AND srtpg.pw_end_date
                     AND st3.week_type = srtpg.week_type
              WHERE st3.week_type = 'Promotion Week'
                    AND srtpg.week_type = 'Promotion Week'
                    --and srtpg.time_period_group like 'Last Week%'
                    AND st3.crn != '000'
                    AND st3.total_sales > 0
              GROUP BY 1, 2, 3, 4, srtpg.pw_end_date)


      SELECT
        metric_type,
        time_period_group,
        store_nbr,
        count(DISTINCT crn)
      FROM active_members
      WHERE row_nbr = 1
      GROUP BY metric_type, time_period_group, store_nbr;

    Insert into loyalty_bi_analytics.smd_temp4b
    select tpg.time_period_group, tpg.metric_type,
    f.store_nbr,
    f.online_flag,
    f.life_stage,
    f.affluence,
    f.valuable_flag,
    f.Crn,
    sum(total_sales) as total_sales,
    sum(total_sales_scan_lylty) as total_sales_scan,
      --sum(total_sales_scan) as total_sales_scan,
    sum(total_sales_flux) as total_sales_flux,
    sum(basket_count) as basket_count,
    sum(basket_count_scan_lylty) as basket_count_scan,
      --sum(total_sales_scan) as total_sales_scan,
    sum(basket_count_flux) as basket_count_flux,
    sum(prod_qty) as prod_qty,
    sum(prod_qty_scan) as prod_qty_scan,
    sum(prod_qty_flux) as prod_qty_flux,
    avg(sow) as sow, --available when sow is in fact table
    count(distinct tpg.pw_end_date)
    ,sum(tag_total_sales) as tag_rate_total_sales
    ,sum(tag_total_sales_scan) as tag_rate_total_sales_scan
    ,sum(scan_rate_total_count) as scan_rate_basket_count
    ,sum(scan_rate_total_scan_count) as scan_rate_basket_count_scan
    ,f.week_type
    from
    loyalty_bi_analytics.smd_temp3 f
    inner join loyalty_bi_analytics.smd_rel_time_period_group tpg
    on f.pw_end_date = tpg.pw_end_date
    and f.week_type = tpg.week_type

    group by 1,2,3,4,5,6,7,8,24     ;

    INSERT INTO loyalty_bi_analytics.smd_first_scan
    with reg as
    (
    select lylty_card_nbr, crn,  max(lylty_card_rgstr_time) as reg_time
    from loyalty_bi_analytics.dim_lylty_card_hist_v_bi
    where lylty_card_status_desc = 'Active With Details'
    and lylty_card_rgstr_time <> '2099-12-31 00:00:00'
    --and lylty_card_nbr  = '1389876740155254685'
    group by 1,2
    ),
    shop as
    (
    select reg.lylty_card_nbr, reg.crn, ass.start_txn_time, ass.store_nbr,
    row_number() over (partition by reg.lylty_card_nbr, reg.crn order by ass.start_txn_time asc) rono
    from basket_sales_summary ass
    inner join reg
    on ass.lylty_card_nbr = reg.lylty_card_nbr
    and ass.start_txn_time > reg.reg_time
    )
    select trunc(dd.fw_end_date) as pw_end_date, store_nbr, count(distinct crn) as first_scan_count
    from shop
    inner join dim_date dd
    on trunc(shop.start_txn_time) = dd.clndr_date
    where rono = 1
    group by 1,2
    ;

    insert into loyalty_bi_analytics.smd_unreg
    with cards as
    (
    select lylty_card_nbr,  max(exp_time) as reg_time
    from loyalty_bi_analytics.dim_lylty_card_hist_v_bi
    where lylty_card_status_desc = 'Active Without Details'
    group by 1
    )
    select distinct trunc(dd.fw_end_date) as pw_end_date, ass.store_nbr, cards.lylty_card_nbr
    from basket_sales_summary ass
    inner join cards
    on ass.lylty_card_nbr = cards.lylty_card_nbr
    and ass.start_txn_time < cards.reg_time
    inner join dim_date dd
    on trunc(ass.start_txn_time) = dd.clndr_date
    ;




 ;
    insert into loyalty_bi_analytics.smd_comp
    with comp_dates as (
           SELECT tpg.time_period_group, tpg.pw_end_date, tpg2.pw_end_date AS Comp_date,tpg.week_type
    FROM (SELECT *, rank() over (PARTITION BY tpg.time_period_group ORDER BY pw_end_date) AS day_id FROM loyalty_bi_analytics.smd_rel_time_period_group tpg WHERE metric_type = 'TP') tpg
    INNER JOIN (SELECT *, rank() over (PARTITION BY tpg.time_period_group ORDER BY pw_end_date) AS day_id FROM loyalty_bi_analytics.smd_rel_time_period_group tpg WHERE metric_type = 'LP') tpg2
    ON tpg.time_period_group = tpg2.time_period_group
    AND tpg.day_id = tpg2.day_id
    and tpg.week_type = tpg2.week_type
    ),
    store_exl_flag_TP as (
                  select
    temp4.time_period_group
    ,temp4.week_type
    ,case when len(life_stage) = 0 then 'UNCLASSIFIED' else life_stage end as life_stage
    ,case when len(affluence) = 0 then 'UNCLASSIFIED' else affluence end as affluence
    ,temp4.store_nbr
    ,valuable_flag
    ,sum(total_sales) as total_sales
                                ,sum(total_sales_scan) as total_sales_scan
                                ,sum(total_sales_flux) as total_sales_flux

                  from loyalty_bi_analytics.smd_temp4 temp4 left join
                         (
                             /*list of stores to exclude based on each time_period_group*/
                                 select distinct a.time_period_group, store.store_nbr,a.week_type
                             FROM
                             (select time_period_group, week_type,max(pw_end_date) as max_clndr_Date from comp_dates group by 1,2) a
                             inner join
                             (select time_period_group, week_type,min(pw_end_date) - 6 as min_clndr_Date from comp_dates group by 1,2) b
                             on a.time_period_group = b.time_period_group
                               and a.week_type = b.week_type
                             left join loyalty_bi_analytics.member_comp_Excl_Store_details store
                             on not ( (store.effective_date >=  a.max_clndr_Date)  or (store.effective_to<= b.min_clndr_Date))
                         ) x
                         on  temp4.time_period_group = x.time_period_group
                         and temp4.store_nbr = x.store_nbr
                         and temp4.week_type = x.week_type
                         where x.store_nbr is null
                         and temp4.metric_type = 'TP'
        group by 1,2,3,4,5,6
           )

    ,
      store_exl_flag_LP as (
                                select
    temp4.time_period_group
    ,temp4.week_type
    ,life_stage
    ,affluence
    ,temp4.store_nbr
    ,valuable_flag
    ,sum(total_sales) as total_sales
                                ,sum(total_sales_scan) as total_sales_scan
                                ,sum(total_sales_flux) as total_sales_flux
                  from loyalty_bi_analytics.smd_temp4 temp4 left join
                         (
                             /*list of stores to exclude based on each time_period_group*/
                                 select distinct a.time_period_group, store.store_nbr,a.week_type
                             FROM
                             (select time_period_group, week_type,max(Comp_date) as max_clndr_Date from comp_dates group by 1,2) a
                             inner join
                             (select time_period_group, week_type,min(Comp_date) - 6 as min_clndr_Date from comp_dates group by 1,2) b
                             on a.time_period_group = b.time_period_group
                               and a.week_type = b.week_type
                             left join loyalty_bi_analytics.member_comp_Excl_Store_details store
                             on not ( (store.effective_date >=  a.max_clndr_Date)  or (store.effective_to<= b.min_clndr_Date))
                         ) x
                         on  temp4.time_period_group = x.time_period_group
                         and temp4.store_nbr = x.store_nbr
                         and temp4.week_type = x.week_type
                         where x.store_nbr is null
                         and temp4.metric_type = 'LP'
        group by 1,2,3,4,5,6
           )


    select
     a.time_period_group
    ,a.week_type
    ,a.life_stage
    ,a.affluence
    ,a.store_nbr
    ,NULL
    ,a.valuable_flag
    ,NULL
    ,a.total_sales as total_sales_comp
    ,b.total_sales as total_sales_comp_LY
    ,a.total_sales_scan as total_sales_scan
    ,b.total_sales_scan as total_sales_scan_ly
    ,a.total_sales_flux as total_sales_flux
    ,b.total_sales_flux as total_sales_flux_ly
    from store_exl_flag_TP a
    inner join store_exl_flag_LP b
    on  a.time_period_group = b.time_period_group
    and a.store_nbr = b.store_nbr
    and a.life_stage = b.life_stage
    and a.affluence = b.affluence
    and a.week_type = b.week_type
    and a.valuable_flag = b.valuable_flag;



    insert into loyalty_bi_analytics.smd_sales
    with comp_dates as (
       SELECT tpg.time_period_group, tpg.pw_end_date, tpg2.pw_end_date AS Comp_date,tpg.week_type
    FROM (SELECT *, rank() over (PARTITION BY tpg.time_period_group ORDER BY pw_end_date) AS day_id FROM loyalty_bi_analytics.smd_rel_time_period_group tpg WHERE metric_type = 'TP') tpg
    INNER JOIN (SELECT *, rank() over (PARTITION BY tpg.time_period_group ORDER BY pw_end_date) AS day_id FROM loyalty_bi_analytics.smd_rel_time_period_group tpg WHERE metric_type = 'LP') tpg2
    ON tpg.time_period_group = tpg2.time_period_group
    AND tpg.day_id = tpg2.day_id
    and tpg.week_type = tpg2.week_type
    )
    ,data as
    (
    SELECT distinct time_period_group
       , week_type
       , metric_type
       , t4.store_nbr
       , zone_name
       , grouparea_name
       , stateshort
       ,sum(total_sales) over (partition by time_period_group, metric_type,week_type, t4.store_nbr) as store_total_sales
        -- , sum(total_sales) over (partition by time_period_group, metric_type, grouparea_name) as grouparea_total_sales
        --  , sum(total_sales) over (partition by time_period_group, metric_type, zone_name) as zone_total_sales
       , case when sum(total_sales) over (partition by time_period_group, metric_type,week_type, t4.store_nbr)=0 then null
          else sum(total_sales_scan) over (partition by time_period_group, metric_type,week_type, t4.store_nbr) /
         sum(total_sales) over (partition by time_period_group, metric_type,week_type, t4.store_nbr) end as store_tag_rate,
          case when sum(basket_count) over (partition by time_period_group, metric_type,week_type, t4.store_nbr) = 0 then null
        else sum(basket_count_scan) over (partition by time_period_group, metric_type, week_type,t4.store_nbr) * 1.0 /
         sum(basket_count) over (partition by time_period_group, metric_type,week_type, t4.store_nbr) end as store_scan_rate
        from datacap.loyalty_bi_analytics.smd_temp4 t4
        inner join loyalty_bi_analytics.smd_dim_store s
        on t4.store_nbr = s.store_nbr

    )

  ,
  data_exl_flag_TP as (
              select data.*,
                     case when x.store_nbr is not null then 1 else 0 end as exl_store_flag
              from data left join
                     (
                         /*list of stores to exclude based on each time_period_group*/
                             select a.time_period_group, store.store_nbr,a.week_type
                         FROM
                         (select time_period_group, week_type,max(pw_end_date) as max_clndr_Date from comp_dates group by 1,2) a
                         inner join
                         (select time_period_group, week_type,min(pw_end_date) - 6 as min_clndr_Date from comp_dates group by 1,2) b
                         on a.time_period_group = b.time_period_group
                           and a.week_type = b.week_type
                         left join loyalty_bi_analytics.member_comp_Excl_Store_details store
                         on not ( (store.effective_date >=  a.max_clndr_Date)  or (store.effective_to<= b.min_clndr_Date))
		             ) x
                     on  data.time_period_group = x.time_period_group
                     and data.store_nbr = x.store_nbr
                     and data.metric_type = 'TP'
                     and data.week_type = x.week_type
       )

    ,
       data_exl_flag_LP as (
              select data.*,
                     case when x.store_nbr is not null then 1 else 0 end as exl_store_flag
              from data left join
                     (
                         /*list of stores to exclude based on each time_period_group*/
                             select a.time_period_group, store.store_nbr,a.week_type
                         FROM
                         (select time_period_group, week_type,max(pw_end_date) as max_clndr_Date from comp_dates group by 1,2) a
                         inner join
                         (select time_period_group, week_type,min(pw_end_date) - 6 as min_clndr_Date from comp_dates group by 1,2) b
                         on a.time_period_group = b.time_period_group
                           and a.week_type = b.week_type
                         left join loyalty_bi_analytics.member_comp_Excl_Store_details store
                         on not ( (store.effective_date >=  a.max_clndr_Date)  or (store.effective_to<= b.min_clndr_Date))
		             ) x
                     on  data.time_period_group = x.time_period_group
                     and data.store_nbr = x.store_nbr
                     and data.metric_type = 'LP'
                     and data.week_type = x.week_type
       )

    ,
  rank_stage as (
    select d1.*
       , case when d2.store_nbr is null or d1.exl_store_flag =1 or d2.exl_store_flag = 1 then -99
         when d2.store_total_sales = 0 then -99
         when d2.store_total_sales != 0 then (d1.store_total_sales - d2.store_total_sales)/d2.store_total_sales end as comp_growth
       , d1.exl_store_flag TP_X
       , d2.exl_store_flag LP_X
    from data_exl_flag_TP d1 left outer join data_exl_flag_LP d2
    on d1.store_nbr = d2.store_nbr
    and d1.time_period_group = d2.time_period_group
    and d1.week_type = d2.week_type
    and d1.metric_type!=d2.metric_type
  )
    ,
    rank as
    (
    select d1.time_period_group
       , d1.metric_type
       , d1.store_nbr
       , d1.zone_name
       , d1.grouparea_name
       , d1.week_type
       , case when d1.comp_growth = -99 then count(d1.store_nbr) over (partition by d1.time_period_group,d1.stateshort, d1.metric_type, d1.zone_name,d1.week_type) ELSE
          rank() over (partition by d1.time_period_group, d1.metric_type, d1.zone_name,d1.stateshort,d1.week_type order by d1.comp_growth desc) end as zone_rank_total_sales
       , case when d1.comp_growth = -99 then count(d1.store_nbr) over (partition by d1.time_period_group, d1.metric_type, d1.grouparea_name,d1.stateshort,d1.week_type) else
          rank() over (partition by d1.time_period_group, d1.metric_type, d1.grouparea_name,d1.stateshort,d1.week_type order by d1.comp_growth desc) end  as grouparea_rank_total_sales
       , rank() over (partition by d1.time_period_group, d1.metric_type, d1.zone_name,d1.stateshort,d1.week_type order by d1.store_tag_rate desc) as zone_rank_tag_rate
       , rank() over (partition by d1.time_period_group, d1.metric_type, d1.grouparea_name,d1.stateshort,d1.week_type order by d1.store_tag_rate desc) as grouparea_rank_tag_rate
       , rank() over (partition by d1.time_period_group, d1.metric_type, d1.zone_name,d1.stateshort,d1.week_type order by d1.store_scan_rate desc) as zone_rank_scan_rate
       , rank() over (partition by d1.time_period_group, d1.metric_type, d1.grouparea_name,d1.stateshort,d1.week_type order by d1.store_scan_rate desc) as grouparea_rank_scan_rate
       , d1.comp_growth as comp_growth
       , d1.TP_X
       , d1.LP_X
    from rank_stage d1

    )

    SELECT
   t4.time_period_group
       , t4.metric_type
       , t4.week_type
       , t4.store_nbr
       , online_flag
       , pickup_flag
       , life_stage
       , affluence
       , valuable_flag
       , shopper_count
       , total_sales
       , total_sales_scan
       , tag_rate_total_sales
       , tag_rate_total_sales_scan
       , total_sales_flux
       , basket_count
       , basket_count_scan
       , scan_rate_basket_count
       , scan_rate_basket_count_scan
       , basket_count_flux
       , prod_qty
       , prod_qty_scan
       , prod_qty_flux
       , sow
       , nbr_of_weeks
       , case when t4.metric_type = 'TP' then zone_rank_total_sales else NULL END
       , case when t4.metric_type = 'TP' then grouparea_rank_total_sales else NULL END
       , case when t4.metric_type = 'TP' then zone_rank_tag_rate else NULL END
       , case when t4.metric_type = 'TP' then grouparea_rank_tag_rate else NULL END
       , case when t4.metric_type = 'TP' then zone_rank_scan_rate else NULL END
       , case when t4.metric_type = 'TP' then grouparea_rank_scan_rate else NULL END
    FROM datacap.loyalty_bi_analytics.smd_temp4 t4
    left join rank
    on t4.time_period_group = rank.time_period_group
    and t4.metric_type = rank.metric_type
    and t4.store_nbr = rank.store_nbr
    and t4.week_type = rank.week_type

    ;



    insert into loyalty_bi_analytics.smd_mapping_mx_Cnt1
    with cnt as
    (
    select
    a.store_nbr, a.banner, a.stateshort, a.storename, a.latitude, a.longitude, a.sa1_maincode_2016, a.sa1_7digitcode_2016, a.sa2_maincode_2016, a.sa2_5digitcode_2016
    , t4.time_period_group
    , t4.crn
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group  ORDER BY t4.crn)  as customer_count_national --equal to count(distinct ) over partition by
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group, a.stateshort  ORDER BY t4.crn)  as customer_count_state --equal to count(distinct ) over partition by
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group, a.sa2_maincode_2016  ORDER BY t4.crn)  as customer_count_s2
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group, a.sa1_maincode_2016  ORDER BY t4.crn)  as customer_count_s1
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group, a.store_nbr  ORDER BY t4.crn)  as customer_count_store

    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group  ORDER BY t4.store_nbr)  as store_count_national --equal to count(distinct ) over partition by
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group, a.stateshort  ORDER BY t4.store_nbr)  as store_count_state --equal to count(distinct ) over partition by
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group, a.sa2_maincode_2016  ORDER BY t4.store_nbr)  as store_count_s2
    , DENSE_RANK() OVER(PARTITION BY t4.time_period_group, a.sa1_maincode_2016  ORDER BY t4.store_nbr)  as store_count_s1
    , 1 as store_count_store


    from loyalty_bi_analytics.competitor_location_census_bi a
    left join loyalty_bi_analytics.smd_temp4b t4
    on  a.store_nbr = t4.store_nbr
    and t4.metric_type = 'TP'
    where upper(a.banner) = 'WOOLWORTHS'
      and t4.week_type = 'Financial Week'
    )
    select distinct
    cnt.store_nbr, cnt.banner, cnt.stateshort, cnt.storename, cnt.latitude, cnt.longitude, cnt.sa1_maincode_2016, cnt.sa1_7digitcode_2016, cnt.sa2_maincode_2016, cnt.sa2_5digitcode_2016
    , cnt.time_period_group
    , max(cnt.customer_count_national) OVER(PARTITION BY cnt.time_period_group ) as customer_count_national
    , max(cnt.customer_count_state) OVER(PARTITION BY cnt.time_period_group, cnt.stateshort ) as customer_count_state
    , max(cnt.customer_count_s2) OVER(PARTITION BY cnt.time_period_group, cnt.sa2_maincode_2016 )  as customer_count_s2
    , max(cnt.customer_count_s1) OVER(PARTITION BY cnt.time_period_group, cnt.sa1_maincode_2016 )  as customer_count_s1
    , max(cnt.customer_count_store) OVER(PARTITION BY cnt.time_period_group, cnt.store_nbr )  as customer_count_store

    , max(cnt.store_count_national) OVER(PARTITION BY cnt.time_period_group ) as store_count_national
    , max(cnt.store_count_state) OVER(PARTITION BY cnt.time_period_group, cnt.stateshort ) as store_count_state
    , max(cnt.store_count_s2) OVER(PARTITION BY cnt.time_period_group, cnt.sa2_maincode_2016 )  as store_count_s2
    , max(cnt.store_count_s1) OVER(PARTITION BY cnt.time_period_group, cnt.sa1_maincode_2016 )  as store_count_s1
    , 1 as store_count_store



    from cnt ;


    insert into loyalty_bi_analytics.smd_mapping
    with mx_Cnt as
    (
    select distinct
    cnt.store_nbr, banner, storename, sa2_maincode_2016--, stateshort, storename, latitude, longitude, sa1_maincode_2016, sa1_7digitcode_2016, sa2_maincode_2016, sa2_5digitcode_2016
    , cnt.time_period_group
    , customer_count_national
    , customer_count_state
    , customer_count_s2
    , customer_count_s1
    , customer_count_store
    , store_count_national
    , store_count_state
    , store_count_s2
    , store_count_s1
    , store_count_store
    --, avg(dist_wow ) over (partition by t4.time_period_group, cnt.stateshort) as dist_wow_state
    --, avg(least(dist_coles, dist_aldi , dis_mtcsh)  ) over (partition by t4.time_period_group, cnt.stateshort) as dist_compt_state

    , nvl(avg(lccp.dist_wow ) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as dist_wow_s2
    , nvl(avg(lccp.dist_coles ) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as dist_coles_s2
    , nvl(avg(lccp.dist_aldi ) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as dist_aldi_s2
    , nvl(avg(lccp.dist_mtcsh ) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as dist_mtcsh_s2


    , nvl(avg(least(dist_coles, dist_aldi , dist_mtcsh)  ) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as dist_compt_s2



    , nvl(avg(sow*1.0) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as sow_s1
    , nvl(avg(sow*1.0) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as sow_s2
    , nvl(avg(sow*1.0) over (partition by t4.time_period_group, cnt.stateshort),0) as sow_state

    , nvl(sum(total_sales) over (partition by t4.time_period_group),0) as total_sales_national
    , nvl(sum(total_sales) over (partition by t4.time_period_group, cnt.stateshort),0) as total_sales_state
    , nvl(sum(total_sales) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as total_sales_s2
    , nvl(sum(total_sales) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as total_sales_s1

    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group),0) as total_sales_online_national
    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group, cnt.stateshort),0) as total_sales_online_state
    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as total_sales_online_s2
    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as total_sales_online_s1


    , nvl(sum(total_sales_scan) over (partition by t4.time_period_group),0) as total_sales_scan_national
    , nvl(sum(total_sales_scan) over (partition by t4.time_period_group, cnt.stateshort),0) as total_sales_scan_state
    , nvl(sum(total_sales_scan) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as total_sales_scan_s2
    , nvl(sum(total_sales_scan) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as total_sales_scan_s1

    , nvl(sum(basket_count) over (partition by t4.time_period_group),0) as basket_count_national
    , nvl(sum(basket_count) over (partition by t4.time_period_group, cnt.stateshort),0) as basket_count_state
    , nvl(sum(basket_count) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as basket_count_s2
    , nvl(sum(basket_count) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as basket_count_s1

    , nvl(sum(basket_count_scan) over (partition by t4.time_period_group),0) as basket_count_scan_national
    , nvl(sum(basket_count_scan) over (partition by t4.time_period_group, cnt.stateshort),0) as basket_count_scan_state
    , nvl(sum(basket_count_scan) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as basket_count_scan_s2
    , nvl(sum(basket_count_scan) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as basket_count_scan_s1

    , nvl(sum(t4.tag_rate_total_sales) over (partition by t4.time_period_group),0) as tag_rate_total_sales_national
    , nvl(sum(t4.tag_rate_total_sales) over (partition by t4.time_period_group, cnt.stateshort),0) as tag_rate_total_sales_state
    , nvl(sum(t4.tag_rate_total_sales) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as tag_rate_total_sales_s2
    , nvl(sum(t4.tag_rate_total_sales) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as tag_rate_total_sales_s1

    , nvl(sum(t4.tag_rate_total_sales_scan) over (partition by t4.time_period_group),0) as tag_rate_scan_total_sales_national
    , nvl(sum(t4.tag_rate_total_sales_scan) over (partition by t4.time_period_group, cnt.stateshort),0) as tag_rate_scan_total_sales_state
    , nvl(sum(t4.tag_rate_total_sales_scan) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as tag_rate_scan_total_sales_s2
    , nvl(sum(t4.tag_rate_total_sales_scan) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as tag_rate_scan_total_sales_s1

    , nvl(sum(t4.scan_rate_basket_count) over (partition by t4.time_period_group),0) as scan_rate_basket_count_national
    , nvl(sum(t4.scan_rate_basket_count) over (partition by t4.time_period_group, cnt.stateshort),0) as scan_rate_basket_count_state
    , nvl(sum(t4.scan_rate_basket_count) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as scan_rate_basket_count_s2
    , nvl(sum(t4.scan_rate_basket_count) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as scan_rate_basket_count_s1

    , nvl(sum(t4.scan_rate_basket_count_scan) over (partition by t4.time_period_group),0) as scan_rate_basket_count_scan_national
    , nvl(sum(t4.scan_rate_basket_count_scan) over (partition by t4.time_period_group, cnt.stateshort),0) as scan_rate_basket_count_scan_state
    , nvl(sum(t4.scan_rate_basket_count_scan) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as scan_rate_basket_count_scan_s2
    , nvl(sum(t4.scan_rate_basket_count_scan) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as scan_rate_basket_count_scan_s1


    from loyalty_bi_analytics.smd_mapping_mx_Cnt1 cnt
    left join loyalty_bi_analytics.smd_temp4b t4
    on cnt.store_nbr = t4.store_nbr
    and t4.time_period_group = cnt.time_period_group
    left join loyalty_bi_analytics.location_customer_competitor_proximity lccp
    on t4.crn = lccp.crn
    where t4.metric_type = 'TP'
      and t4.week_type = 'Financial Week'


    )
    ,
     mx_Cnt_LY as
    (
    select distinct
    cnt.store_nbr, banner--, stateshort, storename, latitude, longitude, sa1_maincode_2016, sa1_7digitcode_2016, sa2_maincode_2016, sa2_5digitcode_2016
    , cnt.time_period_group


    , nvl(sum(total_sales) over (partition by t4.time_period_group),0) as total_sales_national_LY
    , nvl(sum(total_sales) over (partition by t4.time_period_group, cnt.stateshort),0) as total_sales_state_LY
    , nvl(sum(total_sales) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as total_sales_s2_LY
    , nvl(sum(total_sales) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as total_sales_s1_LY

    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group),0) as total_sales_online_national_LY
    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group, cnt.stateshort),0) as total_sales_online_state_LY
    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group, cnt.sa2_maincode_2016),0) as total_sales_online_s2_LY
    , nvl(sum(case when t4.online_flag = 1 then total_sales end) over (partition by t4.time_period_group, cnt.sa1_maincode_2016),0) as total_sales_online_s1_LY


    from loyalty_bi_analytics.smd_mapping_mx_Cnt1 cnt
    left join loyalty_bi_analytics.smd_temp4b t4
    on cnt.store_nbr = t4.store_nbr
    and t4.time_period_group = cnt.time_period_group
    where t4.metric_type = 'LP'
    and t4.week_type = 'Financial Week'

    )
    select
    case
    when upper(a.banner) = 'WOOLWORTHS' then a.store_nbr::int
    when upper(a.banner) = 'COLES' then 100000 + a.store_nbr::int
    when upper(a.banner) = 'IGA' then 200000 + a.store_nbr::int
    when upper(a.banner) = 'ALDI' then 300000 + row_number() over (PARTITION BY a.banner, t4.time_period_group order by a.storename)
    end::varchar as store_nbr

    , t4.time_period_group
    , customer_count_national
    , customer_count_state
    , customer_count_s2
    , customer_count_s1
    , customer_count_store
    , dist_wow_s2
    , dist_coles_s2
    , dist_aldi_s2
    , dist_mtcsh_s2
    , dist_compt_s2
    , sow_s1
    , sow_s2
    , sow_state
    , total_sales_national
    , total_sales_state
    , total_sales_s2
    , total_sales_s1
    , nvl(sum(case when metric_type = 'TP' then total_sales end),0) as total_sales
    , nvl(total_sales_national_ly,0)
    , nvl(total_sales_state_ly,0)
    , nvl(total_sales_s2_ly,0)
    , nvl(total_sales_s1_ly,0)
    , nvl(sum(case when metric_type = 'LP' then total_sales end),0) as total_sales_ly
    , nvl(avg(case when metric_type = 'TP' then sow*1.0 end),0) as sow
    , total_sales_online_national
    , total_sales_online_state
    , total_sales_online_s2
    , total_sales_online_s1
    , nvl(sum(case when metric_type = 'TP' and t4.online_flag = 1 then total_sales end),0) as total_sales_online
    , nvl(total_sales_online_national_ly,0)
    , nvl(total_sales_online_state_ly,0)
    , nvl(total_sales_online_s2_ly,0)
    , nvl(total_sales_online_s1_ly,0)
    , nvl(sum(case when metric_type = 'LP' and t4.online_flag = 1 then total_sales end),999999999) as total_sales_online_ly
    , tag_rate_scan_total_sales_national
    , tag_rate_scan_total_sales_state
    , tag_rate_scan_total_sales_s2
    , tag_rate_scan_total_sales_s1
    , nvl(sum(case when metric_type = 'TP' then tag_rate_total_sales_scan end ),0) as tag_rate_total_sales_scan_store
    , tag_rate_total_sales_national
    , tag_rate_total_sales_state
    , tag_rate_total_sales_s2
    , tag_rate_total_sales_s1
    , nvl(sum(case when metric_type = 'TP' then tag_rate_total_sales end ),0) as tag_rate_total_sales_store
    , scan_rate_basket_count_national
    , scan_rate_basket_count_state
    , scan_rate_basket_count_s2
    , scan_rate_basket_count_s1
    , nvl(sum(case when metric_type = 'TP' then scan_rate_basket_count end),0) as scan_rate_basket_count_store
    , scan_rate_basket_count_scan_national
    , scan_rate_basket_count_scan_state
    , scan_rate_basket_count_scan_s2
    , scan_rate_basket_count_scan_s1
    , nvl(sum(case when metric_type = 'TP' then scan_rate_basket_count_scan end),0) as scan_rate_basket_count_scan_store
    , store_count_national
    , store_count_state
    , store_count_s2
    , store_count_s1
    , store_count_store
    from mx_Cnt a
    left join mx_Cnt_LY b
    on a.store_nbr = b.store_nbr
    and a.time_period_group = b.time_period_group
    left join loyalty_bi_analytics.smd_temp4b t4
    on upper(a.banner) = 'WOOLWORTHS'
    and a.store_nbr = t4.store_nbr
    and a.time_period_group = t4.time_period_group
    where upper(a.banner) = 'WOOLWORTHS'
      and t4.week_type = 'Financial Week'
    group by a.store_nbr, a.banner, a.storename--, a.stateshort, , a.latitude, a.longitude, a.sa1_maincode_2016, a.sa1_7digitcode_2016, a.sa2_maincode_2016, a.sa2_5digitcode_2016
    , t4.time_period_group
    , customer_count_national
    , customer_count_state
    , customer_count_s2
    , customer_count_s1
    , customer_count_store
    , dist_wow_s2
    , dist_coles_s2
    , dist_aldi_s2
    , dist_mtcsh_s2
    , dist_compt_s2
    , sow_s1
    , sow_s2
    , sow_state
    , total_sales_national
    , total_sales_state
    , total_sales_s2
    , total_sales_s1
    , total_sales_national_ly
    , total_sales_state_ly
    , total_sales_s2_ly
    , total_sales_s1_ly
    , total_sales_online_national
    , total_sales_online_state
    , total_sales_online_s2
    , total_sales_online_s1
    , total_sales_online_national_ly
    , total_sales_online_state_ly
    , total_sales_online_s2_ly
    , total_sales_online_s1_ly
    , tag_rate_total_sales_national
    , tag_rate_total_sales_state
    , tag_rate_total_sales_s2
    , tag_rate_total_sales_s1
    , tag_rate_scan_total_sales_national
    , tag_rate_scan_total_sales_state
    , tag_rate_scan_total_sales_s2
    , tag_rate_scan_total_sales_s1
    , scan_rate_basket_count_national
    , scan_rate_basket_count_state
    , scan_rate_basket_count_s2
    , scan_rate_basket_count_s1
    , scan_rate_basket_count_scan_national
    , scan_rate_basket_count_scan_state
    , scan_rate_basket_count_scan_s2
    , scan_rate_basket_count_scan_s1
    , store_count_national
    , store_count_state
    , store_count_s2
    , store_count_s1
    , store_count_store

    union all
    select
    case
    when upper(a.banner) = 'WOOLWORTHS' then store_nbr::int
    when upper(a.banner) = 'COLES' then 100000 + store_nbr::int
    when upper(a.banner) = 'IGA' then 200000 + store_nbr::int
    when upper(a.banner) = 'ALDI' then 300000 + row_number() over (PARTITION BY a.banner, t4.time_period_group order by a.storename)
    end::varchar as store_nbr
    , t4.time_period_group
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0
    , 0

    from loyalty_bi_analytics.competitor_location_census_bi a
    cross join (select distinct time_period_group from loyalty_bi_analytics.smd_temp4) t4
    where upper(a.banner) <> 'WOOLWORTHS'
    group by a.store_nbr, a.banner, a.stateshort, a.storename, a.latitude, a.longitude, a.sa1_maincode_2016, a.sa1_7digitcode_2016, a.sa2_maincode_2016, a.sa2_5digitcode_2016
    , t4.time_period_group;

--     insert into loyalty_bi_analytics.smd_dim_store
--     select bi.store_nbr, max(bi.store_name),
--     case
--     when max(bi.state) = 'ACT' then 'Australian Capital Territory'
--     when max(bi.state) = 'NSW' then 'New South Wales'
--     when max(bi.state) = 'NT' then 'Northern Territory'
--     when max(bi.state)= 'QLD' then 'Queensland'
--     when max(bi.state) = 'SA' then 'South Australia'
--     when max(bi.state) = 'TAS' then 'Tasmania'
--     when max(bi.state) = 'VIC' then 'Victoria'
--     when max(bi.state) = 'WA' then 'Western Australia'
--     end as state_name,
--     max(bi.grouparea_name), max(bi.zone_name),
--     'WOOLWORTHS' as banner,
--       coalesce(max(bi.state),max(bi.state))
--       , max(a.latitude)
--       , mAx(a.longitude)
--       , max(a.sa1_maincode_2016)
--       , max(a.sa1_7digitcode_2016)
--       , max(a.sa2_maincode_2016)
--       , max(a.sa2_5digitcode_2016)
--     from loyalty_bi_analytics.location_bi bi
--     left join loyalty_bi_analytics.competitor_location_census_bi a
--     on bi.store_nbr = a.store_nbr
--     and upper(a.banner) = 'WOOLWORTHS'
--     where bi.store_nbr <> -100
--     and division_nbr = 1005
--     group by 1
--     union all
--     select case
--     when upper(a.banner) = 'WOOLWORTHS' then a.store_nbr::int
--     when upper(a.banner) = 'COLES' then 100000 + a.store_nbr::int
--     when upper(a.banner) = 'IGA' then 200000 + a.store_nbr::int
--     when upper(a.banner) = 'ALDI' then 300000 + row_number() over (PARTITION BY a.banner order by a.storename)
--     end as store_nbr,
--     a.storename,
--     case
--     when a.stateshort = 'ACT' then 'Australian Capital Territory'
--     when a.stateshort = 'NSW' then 'New South Wales'
--     when a.stateshort = 'NT' then 'Northern Territory'
--     when a.stateshort = 'QLD' then 'Queensland'
--     when a.stateshort = 'SA' then 'South Australia'
--     when a.stateshort = 'TAS' then 'Tasmania'
--     when a.stateshort = 'VIC' then 'Victoria'
--     when a.stateshort = 'WA' then 'Western Australia'
--     end as state_name, null, null,
--     a.banner, a.stateshort, a.latitude, a.longitude, a.sa1_maincode_2016, a.sa1_7digitcode_2016, a.sa2_maincode_2016, a.sa2_5digitcode_2016
--     from loyalty_bi_analytics.competitor_location_census_bi a
--     where upper(a.banner) <> 'WOOLWORTHS';


    insert into loyalty_bi_analytics.smd_temp4c
    with get_year_month as (
            select dd.fy, dd.pofy
    FROM loyalty_bi_analytics.dim_date dd
    WHERE EXTRACT ( YEAR FROM dd.clndr_date) = EXTRACT ( YEAR FROM getdate() - 7)
    AND EXTRACT ( MONTH FROM dd.clndr_date) = EXTRACT ( MONTH FROM getdate() - 7)
    AND EXTRACT ( DAY FROM dd.clndr_date) = EXTRACT ( DAY FROM getdate() - 7)
    ),
    financial_date_data as
    (
    select dd.fy,dd.pofy,dd.clndr_date
    from loyalty_bi_analytics.dim_date dd
    inner join get_year_month gym
        on (dd.fy = gym.fy and dd.pofy<=gym.pofy-1)
        or (dd.fy  = gym.fy - 1 and dd.pofy >=gym.pofy-1)
    )
    ,date_final as (
    select fdd.fy,fdd.pofy,fdd.clndr_date as financial_date, fdd.clndr_date + 2 as promo_date
        , EXTRACT(month from fdd.clndr_date) as clndr_month, EXTRACT(year from fdd.clndr_date) as clndr_year
    from financial_date_data fdd)
    ,
        df_final_promo as(
    select df.fy,df.pofy,trunc(dd.pw_end_date) as pw_end_date,max(clndr_month) as clndr_month, max(clndr_year) as clndr_year
    from date_final df inner join loyalty_bi_analytics.dim_date dd
    on df.promo_date = dd.clndr_date
    group by df.fy,df.pofy,trunc(dd.pw_end_date))

    ,
      df_final_financial as(
    select df.fy,df.pofy,trunc(dd.fw_end_date) as fw_end_date,max(clndr_month) as clndr_month, max(clndr_year) as clndr_year
    from date_final df inner join loyalty_bi_analytics.dim_date dd
    on df.financial_date = dd.clndr_date
    group by df.fy,df.pofy,trunc(dd.fw_end_date))


    select
     TO_CHAR(median(dff.pw_end_date )::date,'Mon') + '-' + right(round(avg(clndr_year*1.00),0)::VARCHAR(5),2)
    ,'Promotion Week'
    ,st3.store_nbr
    ,sum(st3.total_sales) as total_sales,
    sum(st3.total_sales_scan_lylty) as total_sales_scan,
    sum(st3.total_sales_flux) as total_sales_flux,
    sum(st3.basket_count) as basket_count,
    sum(st3.basket_count_scan_lylty) as basket_count_scan,
    sum(st3.basket_count_flux) as basket_count_flux,
    sum(st3.prod_qty) as prod_qty,
    sum(prod_qty_scan) as prod_qty_scan,
    sum(prod_qty_flux) as prod_qty_flux
    ,avg(sow) --available when fact table has this value
    ,cast (sum(st3.tag_total_sales_scan) as NUMERIC(38,8))  as tag_rate_total_scan
    ,cast (sum(st3.tag_total_sales) as NUMERIC(38,8)) as tag_rate_total_sales
    ,sum(st3.scan_rate_total_count::bigint )as scan_rate_total_count
    ,sum(st3.scan_rate_total_scan_count::bigint )as scan_rate_total_count_scan
    from df_final_promo dff inner join loyalty_bi_analytics.smd_temp3 st3
      on dff.pw_end_date = st3.pw_end_date

    group by dff.fy,dff.pofy,st3.store_nbr

    union

    select
      TO_CHAR(median(fw_end_date)::date,'Mon') + '-' + right(round(avg(clndr_year*1.00),0)::VARCHAR(5),2)
    ,'Financial Week'
    ,st3.store_nbr
    ,sum(st3.total_sales) as total_sales,
    sum(st3.total_sales_scan_lylty) as total_sales_scan,
    sum(st3.total_sales_flux) as total_sales_flux,
    sum(st3.basket_count) as basket_count,
    sum(st3.basket_count_scan_lylty) as basket_count_scan,
    sum(st3.basket_count_flux) as basket_count_flux,
    sum(st3.prod_qty) as prod_qty,
    sum(prod_qty_scan) as prod_qty_scan,
    sum(prod_qty_flux) as prod_qty_flux
    ,avg(sow) --available when fact table has this value
    ,cast (sum(st3.tag_total_sales_scan) as NUMERIC(38,8))  as tag_rate_total_scan
    ,cast (sum(st3.tag_total_sales) as NUMERIC(38,8)) as tag_rate_total_sales
    ,sum(st3.scan_rate_total_count::bigint )as scan_rate_total_count
    ,sum(st3.scan_rate_total_scan_count::bigint )as scan_rate_total_count_scan
    from df_final_financial dff inner join loyalty_bi_analytics.smd_temp3 st3
      on dff.fw_end_date = st3.pw_end_date
    group by dff.fy,dff.pofy,st3.store_nbr

    ;

    insert into loyalty_bi_analytics.smd_rel_time_period_group_month
    with get_year_month as (
            select dd.fy, dd.pofy
    FROM loyalty_bi_analytics.dim_date dd
    WHERE EXTRACT ( YEAR FROM dd.clndr_date) = EXTRACT ( YEAR FROM getdate() - 7)
    AND EXTRACT ( MONTH FROM dd.clndr_date) = EXTRACT ( MONTH FROM getdate() - 7)
    AND EXTRACT ( DAY FROM dd.clndr_date) = EXTRACT ( DAY FROM getdate() - 7)
    ),
    financial_date_data as
    (
    select dd.fy,dd.pofy,dd.clndr_date
    from loyalty_bi_analytics.dim_date dd
    inner join get_year_month gym
        on (dd.fy = gym.fy and dd.pofy <= gym.pofy-1)
        or (dd.fy  =  gym.fy - 1 and dd.pofy>= gym.pofy-1)
    )
    ,date_final as (
    select fdd.fy,fdd.pofy,fdd.clndr_date as financial_date, fdd.clndr_date + 2 as promo_date
        , EXTRACT(month from fdd.clndr_date) as clndr_month, EXTRACT(year from fdd.clndr_date) as clndr_year
    from financial_date_data fdd)

  select TO_CHAR(median(promo_date)::date,'Mon') + '-' + right(round(avg(clndr_year*1.00),0)::VARCHAR(5),2)
    ,max(promo_date) as last_promo_date_of_month
    ,case when EXTRACT ( MONTH FROM getdate() - 7) = 1 then 'Jan'
             when EXTRACT ( MONTH FROM getdate() - 7) = 2 then 'Feb'
             when EXTRACT ( MONTH FROM getdate() - 7) = 3 then  'Mar'
             when EXTRACT ( MONTH FROM getdate() - 7) = 4 then 'Apr'
             when EXTRACT ( MONTH FROM getdate() - 7) = 5 then 'May'
             when EXTRACT ( MONTH FROM getdate() - 7) = 6 then  'Jun'
             when EXTRACT ( MONTH FROM getdate() - 7) = 7 then 'Jul'
             when EXTRACT ( MONTH FROM getdate() - 7) = 8 then 'Aug'
             when EXTRACT ( MONTH FROM getdate() - 7) = 9 then 'Sep'
             when EXTRACT ( MONTH FROM getdate() - 7) = 10 then 'Oct'
            when EXTRACT ( MONTH FROM getdate() - 7) = 11 then 'Nov'
            when EXTRACT ( MONTH FROM getdate() - 7) = 12 then 'Dec' end::varchar(10) + '-' + right(EXTRACT ( YEAR FROM getdate())::VARCHAR(5),2),
            row_number() over (order by max(promo_date))
    from date_final df
    group by fy, pofy;

    insert into loyalty_bi_analytics.smd_member_month
    with marketable as
    (
                    SELECT
                        b.time_period_group,
                        cvm.preferred_store       AS store_nbr,
                        count(DISTINCT a.crn)
                            AS marketable_count,
                        count(cvm.crn)            AS preferred_store_crn_count,
                        avg(1 - stretch_perc_raw) AS sow
                    FROM customer_value_model cvm
                        LEFT JOIN loyalty_bi_analytics.lkp_marketable_crn a
                            ON cvm.pw_end_date = a.pw_end_date
                               AND cvm.crn = a.crn
                        INNER JOIN loyalty_bi_analytics.smd_rel_time_period_group_month b
                            ON cvm.pw_end_date = b.last_pw_end_date
                    GROUP BY 1, 2
    ),
        xjoin as
    (
    select a.time_period_group, b.store_nbr
    from
    (select distinct time_period_group from loyalty_bi_analytics.smd_rel_time_period_group_month ) a
    cross join (select distinct store_nbr from loyalty_bi_analytics.location_bi where division_nbr = 1005 and store_nbr != -100
               and store_nbr in (select distinct store_nbr from loyalty_bi_analytics.smd_temp4c)) b

    )

    select a.time_period_group,a.store_nbr,'Promotion Week', avg(d.marketable_count),avg(d.preferred_store_crn_count)
    from xjoin a
    left join marketable d
    on a.time_period_group = d.time_period_group
    and a.store_nbr = d.store_nbr
    group by 1,2

    union

    select a.time_period_group,a.store_nbr,'Financial Week', avg(d.marketable_count),avg(d.preferred_store_crn_count)
    from xjoin a
    left join marketable d
    on a.time_period_group = d.time_period_group
    and a.store_nbr = d.store_nbr
    group by 1,2;

    insert into loyalty_bi_analytics.smd_sales_month
    select
        srtpgm.sort_key
        ,srtpgm.current_month
        ,nvl(st4c.time_period_group,smm.time_period_group)
        ,nvl(st4c.week_type,smm.week_type)
        ,nvl(st4c.store_nbr,smm.store_nbr)
        ,nvl(st4c.total_sales,0)
        ,nvl(st4c.total_sales_scan,0)
        ,nvl(st4c.total_sales_flux,0)
        ,nvl(st4c.basket_count,0)
        ,nvl(st4c.basket_count_scan,0)
        ,nvl(st4c.basket_count_flux,0)
        ,nvl(st4c.prod_qty,0)
        ,nvl(st4c.prod_qty_scan,0)
        ,nvl(st4c.prod_qty_flux,0)
        ,nvl(st4c.sow,0)
        ,smm.marketable_count
        ,smm.preferred_store_crn_count
        ,nvl(st4c.tag_rate_total_sales)
        ,nvl(st4c.tag_rate_total_scan)
        ,nvl(st4c.scan_rate_total_count)
        ,nvl(st4c.scan_rate_total_count_scan)
    from loyalty_bi_analytics.smd_member_month smm
    left OUTER join loyalty_bi_analytics.smd_temp4c st4c
    on smm.time_period_group= st4c.time_period_group
    and smm.store_nbr = st4c.store_nbr
    and smm.week_type = st4c.week_type
    inner join loyalty_bi_analytics.smd_rel_time_period_group_month srtpgm
    on smm.time_period_group = srtpgm.time_period_group;

    insert into loyalty_bi_analytics.smd_temp4d
    with weekly as
    (
    select week_type
      ,pw_end_date
      ,store_nbr
      ,crn
      ,valuable_flag,
    sum(total_sales) as total_sales,
    --sum(total_sales_scan) as total_sales_scan,
    sum(total_sales_scan_lylty) as total_sales_scan,
    sum(total_sales_flux) as total_sales_flux,

    sum(basket_count) as basket_count,
    --sum(basket_count_scan) as basket_count_scan,
    sum(basket_count_scan_lylty) as basket_count_scan,
    sum(basket_count_flux) as basket_count_flux,
    sum(prod_qty) as prod_qty,
    sum(prod_qty_scan) as prod_qty_scan,
    sum(prod_qty_flux) as prod_qty_flux,
    avg(sow) as sow --available when sow is in fact table

    from loyalty_bi_analytics.smd_temp3
    --where total_sales>0
    group by 1,2,3,4,5
    )
    select
        tpg.time_period_group
      , f.week_type
      , f.store_nbr
      , f.crn +'-'+ f.pw_end_date
      , f.valuable_flag,
    sum(total_sales_flux) as total_sales_flux,
    sum(basket_count_flux) as basket_count_flux
    from
    weekly f
    inner join loyalty_bi_analytics.smd_rel_time_period_group tpg
    on f.pw_end_date = tpg.pw_end_date
    and f.week_type = tpg.week_type
        inner join loyalty_bi_analytics.vw_smd_dim_store vsds
        on f.store_nbr = vsds.store_nbr
        where tpg.metric_type = 'TP'
        and f.crn != '000'
    group by 1,2,3,4,5;


    insert into loyalty_bi_analytics.smd_scan_tag_daily
      select b.time_period_group,
        b.metric_type,
        a.start_txn_date,
        b.pw_end_date,
        b.week_type,
        a.store_nbr,
        a.self_service_flag,
        a.tag_total_sales,
        a.tag_total_sales_scan,
        a.scan_rate_total_count,
        a.scan_rate_total_scan_count

    from loyalty_bi_analytics.smd_temp5 a
    inner join loyalty_bi_analytics.smd_rel_time_period_group_day b
    on a.start_txn_date = b.cclndr_date
      and b.week_type = 'Financial Week'


    union


      select b.time_period_group,
        b.metric_type,
        a.start_txn_date,
        b.pw_end_date,
        b.week_type,
        a.store_nbr,
        a.self_service_flag,
        a.tag_total_sales,
        a.tag_total_sales_scan,
        a.scan_rate_total_count,
        a.scan_rate_total_scan_count
    from loyalty_bi_analytics.smd_temp5 a
    inner join loyalty_bi_analytics.smd_rel_time_period_group_day b
    on a.start_txn_date = b.cclndr_date
    and b.week_type = 'Promotion Week' ;

     insert into loyalty_bi_analytics.smd_scan_tag_by_service
            with time_periods as (select
     a.*
    from loyalty_bi_analytics.smd_rel_time_period_group_day a
    inner join loyalty_bi_analytics.smd_rel_time_period_group b
      on a.pw_end_date = b.pw_end_date
    and a.time_period_group = b.time_period_group
    and a.metric_type = b.metric_type
    and a.week_type = b.week_type)

      select b.time_period_group,
        b.metric_type,
        b.week_type,
        a.store_nbr,
        a.self_service_flag,
        sum(a.tag_total_sales),
        sum(a.tag_total_sales_scan),
        sum(a.scan_rate_total_count),
        sum(a.scan_rate_total_scan_count)

    from loyalty_bi_analytics.smd_temp5 a
    inner join time_periods b
    on a.start_txn_date = b.cclndr_date
      and b.week_type = 'Financial Week'
    group by 1,2,3,4,5

    union


      select b.time_period_group,
        b.metric_type,
        b.week_type,
        a.store_nbr,
        a.self_service_flag,
         sum(a.tag_total_sales),
        sum(a.tag_total_sales_scan),
        sum(a.scan_rate_total_count),
        sum(a.scan_rate_total_scan_count)
    from loyalty_bi_analytics.smd_temp5 a
    inner join time_periods b
    on a.start_txn_date = b.cclndr_date
    and b.week_type = 'Promotion Week'
    group by 1,2,3,4,5
;

INSERT INTO LOYALTY_BI_ANALYTICS.smd_member
      WITH first_scan AS
      (
              SELECT
                b.time_period_group,
                b.metric_type,
                a.store_nbr,
                sum(first_scan_count) AS first_scan_count
              FROM loyalty_bi_analytics.smd_first_scan a
                INNER JOIN loyalty_bi_analytics.smd_rel_time_period_group b
                  ON a.pw_end_date = b.pw_end_date
              GROUP BY 1, 2, 3
      ),
          unreg AS
        (
                SELECT
                  b.time_period_group,
                  b.metric_type,
                  a.store_nbr,
                  count(DISTINCT lylty_card_nbr) AS unreg_count
                FROM loyalty_bi_analytics.smd_unreg a
                  INNER JOIN loyalty_bi_analytics.smd_rel_time_period_group b
                    ON a.pw_end_date = b.pw_end_date
                GROUP BY 1, 2, 3
        ),
          marketable AS
        (
                SELECT
                  b.time_period_group,
                  b.metric_type,
                  cvm.preferred_store       AS store_nbr,
                  count(DISTINCT a.crn)     AS marketable_count,
                  count(DISTINCT cvm.crn)   AS preferred_store_crn_count,
                  avg(1 - stretch_perc_raw) AS sow
                FROM customer_value_model cvm
                  LEFT JOIN loyalty_bi_analytics.lkp_marketable_crn a
                    ON cvm.pw_end_date = a.pw_end_date
                       AND cvm.crn = a.crn
                  INNER JOIN loyalty_bi_analytics.smd_rel_time_period_group b
                    ON cvm.pw_end_date = b.pw_end_date - 7
                GROUP BY 1, 2, 3
        )
        ,
          xjoin AS
        (
                SELECT
                  a.time_period_group,
                  a.metric_type,
                  b.store_nbr
                FROM
                  (SELECT DISTINCT
                     time_period_group,
                     metric_type
                   FROM loyalty_bi_analytics.smd_rel_time_period_group) a
                  CROSS JOIN (SELECT DISTINCT store_nbr
                              FROM loyalty_bi_analytics.location_bi
                              WHERE division_nbr = 1005 AND store_nbr != -100
                                    AND store_nbr IN (SELECT DISTINCT store_nbr
                                                      FROM loyalty_bi_analytics.smd_sales)) b

        )
      SELECT
        a.time_period_group,
        a.metric_type,
        a.store_nbr,
        avg(b.first_scan_count),
        avg(c.unreg_count),
        avg(d.marketable_count),
        sum(act.active_member_count),
        avg(d.preferred_store_crn_count),
        avg(d.sow)
      FROM xjoin a
        LEFT JOIN first_scan b
          ON a.time_period_group = b.time_period_group
             AND a.metric_type = b.metric_type
             AND a.store_nbr = b.store_nbr
        LEFT JOIN unreg c
          ON a.time_period_group = c.time_period_group
             AND a.metric_type = c.metric_type
             AND a.store_nbr = c.store_nbr
        LEFT JOIN marketable d
          ON a.time_period_group = d.time_period_group
             AND a.metric_type = d.metric_type
             AND a.store_nbr = d.store_nbr
        LEFT JOIN loyalty_bi_analytics.smd_active_member act
          ON a.time_period_group = act.time_period_group
             AND a.metric_type = act.metric_type
             AND a.store_nbr = act.store_nbr
      GROUP BY 1, 2, 3;


 ;



update loyalty_bi_analytics.sd_data_loaded set "desc" = 'Store-dash FW Completed';


    truncate table loyalty_bi_analytics.smd_temp4;
    --truncate table loyalty_bi_analytics.smd_active_member;
    truncate table loyalty_bi_analytics.smd_temp4b;
    truncate table loyalty_bi_analytics.smd_first_scan ;
    truncate table loyalty_bi_analytics.smd_unreg ;
    truncate table loyalty_bi_analytics.smd_mapping_mx_Cnt1;





